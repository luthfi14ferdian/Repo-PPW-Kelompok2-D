from django.db import models

# Create your models here.
class DataProfile(models.Model):
    Name = models.CharField(max_length=250, default="No Name")
    Birthday = models.CharField(max_length=25, default="1 Januari 1990")
    Gender = models.CharField(max_length=15, default="Not Specified")
    Expertise = models.TextField(max_length=200, default="No Expert")
    Description = models.CharField(max_length=200, default="No Description")
    Email = models.CharField(max_length=200, default="...@gmail.com")
