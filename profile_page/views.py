from django.shortcuts import render
from datetime import datetime, date
from .models import DataProfile

# Create your views here.
mhs_name = 'Saffana Dira Qanya'
user_name = 'Luke Skywalker'
birth_date = date(1998,12,31)
birthdate = birth_date.strftime('%d %B %Y')
gender = 'Male'
desc_user = ' i am a human male Jedi Master who was instrumental in defeating the Galactic Empire and the Sith during the Galactic Civil War. '
exp_user = ["Force User", "Light-Saber mastery", "X-wing starfighter pilot"]
email = 'Luke.skywalker@rebellion.com'
profile = DataProfile(Name = user_name, Birthday = birthdate, Gender= gender, Description = desc_user, Expertise = exp_user, Email = email)

def index(request):
    response= {'profile': profile, 'author' : mhs_name}
    return render(request, 'profile/profile.html', response)
