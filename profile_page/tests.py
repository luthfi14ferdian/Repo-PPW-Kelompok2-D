from django.test import TestCase, Client
from django.urls import resolve
from .views import index, user_name, birthdate, gender, desc_user, exp_user, email
import unittest

# Create your tests here.
class ProfileTes(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, index)

    def test_data_profile_is_exist(self):
        self.assertIsNotNone(user_name)
        self.assertIsNotNone(birthdate)
        self.assertIsNotNone(gender)
        self.assertIsNotNone(email)

    def test_description_is_exist(self):
        self.assertIsNotNone(desc_user)
        self.assertTrue(len(desc_user) >= 5)

    def test_expertise_is_exist(self):
        self.assertIsNotNone(exp_user)
        self.assertTrue(len(exp_user) >= 3)
