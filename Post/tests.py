from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.urls import resolve
from .views import index
from .models import MakePost
from .forms import Message_Form

# Create your tests here.
class UpdateStatusTest(TestCase):
	def test_update_status_url_is_exist(self):
		response = Client().get('/Post/')
		self.assertEqual(response.status_code, 200)

	def test_update_status_using_index_func(self):
		found = resolve('/Post/')
		self.assertEqual(found.func, index)

	def test_model_can_create_new_post(self):
		new_post = MakePost.objects.create(description="First Post")
		counting_all_available_post = MakePost.objects.all().count()
		self.assertEqual(counting_all_available_post,1)

	def test_form_validation_for_blank_post(self):
		form = Message_Form(data={'post': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['post'],
				["Tolong isi post ini"]
		)
	def test_post_fail(self):
		response = Client().post('/Post/posting', {'post':''})
		self.assertEqual(response.status_code, 301)
	def test_lab5_post_success_and_render_the_result(self):
		test = ''
		response_post = Client().post('/Post/posting', {'post': test})
		self.assertEqual(response_post.status_code, 301)
		response= Client().get('/Post/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)