from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import MakePost
# Create your views here.
mhs_name="Pasu Siahaan"
response = {}
def index(request):
    response ['Message_Form']= Message_Form #messsage
    make_post = MakePost.objects.all()
    response['name']='Luke Skywalker'
    response ['author']= mhs_name
    response['make_post']= make_post
    html = 'Post/Post.html'
    return render(request,html,response)
def post(request):
    form = Message_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['make_post'] = request.POST['post']
        make_post = MakePost(description=response['make_post'])
        make_post.save()
        return HttpResponseRedirect('/Post/')
    else:
        return HttpResponseRedirect('/Post/')
