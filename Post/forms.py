from django import forms

class Message_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi post ini',
    }
    post_attrs = {
        'cols': 160,
        'rows': 4,
        'class': 'todo-form-textarea',
        'placeholder':'Apa yang anda pikirkan?'
    }



    post = forms.CharField(error_messages=error_messages,label='', required=True, widget=forms.Textarea(attrs=post_attrs))