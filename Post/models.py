from django.db import models

# Create your models here.
class MakePost(models.Model):
	description = models.CharField(max_length=1600)
	created_date = models.DateTimeField(auto_now_add=True)