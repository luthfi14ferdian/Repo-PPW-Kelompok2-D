from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .forms import Add_Friend_Form
from .models import Friends
from .views import index, add_friend

# Create your tests here.
class AddFriendUnitTest(TestCase):
	def test_add_friend_url_is_exist(self):
		response = Client().get('/Add-Friends/')
		self.assertEqual(response.status_code, 200)

	def test_models_can_create_new_friend(self):
		new_activity = Friends.objects.create(name='Luthfi',link='http://luhtfiferdian.herokuapp.com')
		counting_all_available_friend = Friends.objects.all().count()
		self.assertEqual(counting_all_available_friend,1)

	def test_form_validation_for_blank_items(self):
		form = Add_Friend_Form(data={'name': '', 'link': ''})
		self.assertFalse(form.is_valid())

	def test_friend_showing_all_messages(self):
		name_james = 'James'
		url_james = 'james.oxford.ac.uk'
		data_james = {'name': name_james, 'link': url_james}
		post_data_james = Client().post('/Add-Friends/', data_james)
		self.assertEqual(post_data_james.status_code, 200)