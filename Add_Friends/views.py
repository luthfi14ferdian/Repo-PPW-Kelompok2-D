from django.shortcuts import render, redirect
from .models import Friends
from .forms import Add_Friend_Form
from django.http import HttpResponseRedirect
import pytz
import json

# Create your views here.
my_friends = {}
mhs_name = 'Luthfi Ferdian'
response = {}
def index(request):
    my_friends = Friends.objects.all().values()
    response['add_friend_form'] = Add_Friend_Form
    response['my_friends'] = my_friends
    response['author'] = mhs_name
    return render(request, 'Add_Friends.html', response)


def add_friend(request):
    form = Add_Friend_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        response['link'] = request.POST['link'] if request.POST['link'] != "" else "Anonymous"
        friend = Friends(name=form.cleaned_data['name'], link=form.cleaned_data['link'])
        friend.save()
        return HttpResponseRedirect('/Add-Friends/')
    else:
        response['form'] = form
    html ='friend_result.html'
    friend = Friends.objects.all()
    response['friend'] = friend
    return HttpResponseRedirect('/Add-Friends/')
