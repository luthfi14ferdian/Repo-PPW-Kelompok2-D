from django.db import models

class Friends(models.Model):
	name = models.CharField(max_length = 60)
	link = models.URLField(max_length = 200)
	time = models.DateTimeField(auto_now_add=True)

# Create your models here.
