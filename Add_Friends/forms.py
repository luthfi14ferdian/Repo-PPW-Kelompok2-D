from django import forms

class Add_Friend_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan link',
    }
    name_attrs = {
        'class': 'form-control',
        'placeholder': 'your friend name'
    }
    link_attrs = {
        'class': 'form-control',
        'placeholder': 'your firend link'
    }

    name = forms.CharField(label='Name',required=True, max_length=90,error_messages=error_messages, 
    	widget=forms.TextInput(attrs=name_attrs))
    link = forms.CharField(label='Link',required= False,max_length=90,error_messages=error_messages, 
    	widget=forms.TextInput(attrs=link_attrs))
