from django.test import TestCase
from django.test import TestCase, Client
from django.urls import resolve

from Post.models import MakePost

from stats.views import index

class StatusTest(TestCase):

    def test_Stats_url_is_exist(self):
        post = MakePost.objects.create(
            description="test123"
        )
        response = Client().get('/stats/')
        self.assertEqual(response.status_code,200)

    def test_no_Post(self):
        response = Client().get('/stats/')
        self.assertEqual(response.status_code,200)

    def test_Stats_using_index_func(self):
        found = resolve('/stats/')
        self.assertEqual(found.func, index)
