from django.shortcuts import render
from Post.models import MakePost
from Add_Friends.models import Friends
from profile_page.models import DataProfile

response = {}
def index(request):
    response['Name'] = "Luke Skywalker"
    response['totalFriends']=len(Friends.objects.all())
    stat=MakePost.objects.all().order_by('-created_date')
    jumlahPost=len(MakePost.objects.all())
    response['totalPost']=jumlahPost
    jumlahPost=len(stat)
    if (jumlahPost!=0):
        response['recentPost']=stat[0]
        response['totalPost']=jumlahPost
    else:
        response['totalPost']=0

    return render(request, 'stats/stats.html', response)
